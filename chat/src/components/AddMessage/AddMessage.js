import React from 'react';
import './AddMessage.css';
const AddMessage = props => {
    return (
        <div className="add-message-block">
            <input type="text" value={props.valueUser} className="user-name" onChange={props.changeUserNameHandler} placeholder="Enter your name"/>
            <input type="text" value={props.valueMessage} className="user-message" onChange={props.enterUserMessageHandler} placeholder="Enter your message"/>
            <button className="send-message" onClick={props.sendMessage}>Send message</button>
        </div>
    );
};

export default AddMessage;