import React from 'react';
import './Chat.css';

const Chat = props => {
    return (
        <div className="chat">
            <span className="user-datetime">{props.date}</span>
            <span className="user-name-item">{props.author}</span>
            <span className="user-message-item">{props.message}</span>
        </div>
    );
};

export default Chat;