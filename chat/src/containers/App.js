import React, { Component } from 'react';
import './App.css';
import AddMessage from "../components/AddMessage/AddMessage";
import Chat from "../components/Chat/Chat";

class App extends Component {
  state = {
      chat: [],
      userName: '',
      userMessage: '',
  };

  changeUserNameHandler = event => {
    this.setState({userName: event.target.value});
  };

  enterUserMessageHandler = event => {
      this.setState({userMessage: event.target.value})
  };

  getMessage = () => {
      const P_URL = "http://146.185.154.90:8000/messages";
      return fetch(P_URL).then(response => {
          if (response.ok) {
              return response.json();
          }
          throw new Error('Something went wrong with request');
      }).then(chat => {
          const messageUpdate = chat.map(chat => ({
              ...chat
          }));
          this.setState({chat: messageUpdate});
      }).catch(error => {
          console.log(error);
      })
  };

  componentDidMount() {
      this.getMessage().then(() => {
          clearInterval(this.interval);
          this.interval = setInterval(() => {
              const lastDate = this.state.chat[this.state.chat.length - 1];
              fetch('http://146.185.154.90:8000/messages?datetime=' + lastDate.datetime)
                  .then(chat => {
                      return chat.json();
                  }).then(response => {
                  const messages = this.state.chat;
                  for(let i = 0; i < response.length; i ++) {
                      messages.push(response[i]);
                  }
                  this.setState({chat: messages})
              }).catch(error => {
                  console.log(error);
              });
          }, 2000)
      });

  }

  sendMessage = () => {
        const P_URL = "http://146.185.154.90:8000/messages";

        const data = new URLSearchParams();
        data.set('message', this.state.userMessage);
        data.set('author', this.state.userName);

        fetch(P_URL, {
            method: 'post',
            body: data,
        }).then(() => {
            this.getMessage();
        });
        this.setState({userName:'', userMessage:''});
  };

  render() {
    return (
      <div className="App">
          <AddMessage
              changeUserNameHandler = {this.changeUserNameHandler}
              enterUserMessageHandler = {this.enterUserMessageHandler}
              sendMessage={() => this.sendMessage()}
              valueUser={this.state.userName}
              valueMessage={this.state.userMessage}
          />
          <div className="chat-block">
              {
                  this.state.chat.map(message => {
                      const date = new Date (message.datetime).toLocaleString('ru-RU');
                      return (
                          <Chat
                              key={message._id}
                              author={message.author}
                              message={message.message}
                              date={date}
                          />
                      )})
              }
          </div>
      </div>
    );
  }
}

export default App;
